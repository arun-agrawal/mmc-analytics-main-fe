# READ THIS FIRST
# READ THIS FIRST
# READ THIS FIRST

If you have cloned this first, ***_DO NOT_*** make commits here. If you are looking to make changes, go to the monorepo for this project.

___

# Mercer App Starter

## Quick start

```bash
# clone our repo
git clone git@bitbucket.org:oliverwymantechssg/ngpd-merceros-ui-starter-kit.git name-of-project

# change directory to our repo
cd name-of-project

# install the repo with npm
npm install

# start the server
npm start
```
go to [http://localhost:4200](http://localhost:4200) in your browser


## Overview

Overview of this frontend goes here.