import { ChangeDetectionStrategy, Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthFacadeService } from 'ngpd-merceros-authentication-fe-components';
import { Subscription } from 'rxjs';

import { englishLabels } from './language-pack/english';
import { deutchLabels } from './language-pack/deutch';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mercer-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnDestroy {
  private _subscription = new Subscription();

  constructor( private _activatedRoute: ActivatedRoute, private _authFacade: AuthFacadeService ) {
    this._authFacade.checkSession();

    if (!localStorage.getItem('language')) {
      localStorage.setItem('language', 'en-US');
      this._authFacade.updateLabels(englishLabels);
    } else {
      if (localStorage.getItem('language') === 'de-DE') {
        this._authFacade.updateLabels(deutchLabels);
      } else {
        this._authFacade.updateLabels(englishLabels);
      }
    }
    this._authFacade.setMfaLocale(localStorage.getItem('language'));
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
