import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MercerOSModule } from 'merceros-ui-components';

import { DragHandleComponent } from './drag-handle/drag-handle.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { SectionMarkerComponent } from './section-marker/section-marker.component';
import { ToastService } from './toast-outlet/services/toast-outlet.service';
import { ToastOutletComponent } from './toast-outlet/toast-outlet.component';

export const SHARED_COMPONENTS = [
  NavigationBarComponent,
  ToastOutletComponent,
  DragHandleComponent,
  SectionMarkerComponent,
];

@NgModule({
  declarations: SHARED_COMPONENTS,
  imports: [CommonModule, RouterModule, MercerOSModule, ReactiveFormsModule],
  exports: SHARED_COMPONENTS,
  providers: [ToastService]
})
export class ComponentsModule {}
