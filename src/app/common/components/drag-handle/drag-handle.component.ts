import { ChangeDetectionStrategy, Component, HostBinding, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mercer-app-drag-handle',
  templateUrl: './drag-handle.component.html',
  styleUrls: ['./drag-handle.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DragHandleComponent implements OnInit {
  @HostBinding('class.mercer-app-c-drag-handle') true;

  constructor() { }

  ngOnInit(): void {
  }

}
