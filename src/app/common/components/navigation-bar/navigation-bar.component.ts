import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { MosBreadcrumb, MosNavigationItem } from 'merceros-ui-components';

@Component({
  selector: 'mercer-app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationBarComponent {
  @Input() breadcrumbData: MosBreadcrumb[] = [
    { label: '', icon: 'home', url: '/' },
    { label: 'Breadcrumb 1', icon: '', url: '/' },
  ];
  @Input() sectionLinkNavigation: MosNavigationItem[] = [
    { path: '/', name: 'Link 1' },
    { path: '/', name: 'Link 2' },
    { path: '/', name: 'Link 3' },
  ];
  /** Whether or not the navigation bar should be hidden from view */
  @Input() hidden = false;
}
