import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionMarkerComponent } from './section-marker.component';

describe('SectionMarkerComponent', () => {
  let component: SectionMarkerComponent;
  let fixture: ComponentFixture<SectionMarkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionMarkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionMarkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
