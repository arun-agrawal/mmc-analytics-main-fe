import { ChangeDetectionStrategy, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mercer-app-section-marker',
  templateUrl: './section-marker.component.html',
  styleUrls: ['./section-marker.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SectionMarkerComponent implements OnInit {
  /** Title of the form field section that is indented with a border-left */
  @Input() title = '';

  constructor() { }

  ngOnInit(): void {
  }

}
