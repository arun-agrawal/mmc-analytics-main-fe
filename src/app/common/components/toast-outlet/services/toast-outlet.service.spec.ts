import { TestBed } from '@angular/core/testing';

import { ToastService } from './toast-outlet.service';

describe('ToastOutletService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ToastService]
  }));

  it('should be created', () => {
    const service: ToastService = TestBed.get(ToastService);
    expect(service).toBeTruthy();
  });
});
