import { EventEmitter, Injectable } from '@angular/core';

import { ToastTypes } from '../../../constants/toast-type/toast-type.constants';
import { ToastOptions } from '../../../models/toast-option/toast-option.models';

@Injectable()
export class ToastService {
  onCreate = new EventEmitter();

  create(options: ToastOptions) {
    if (!options) {
      return;
    }
    const transformedOptions = { ...options } as any;

    if (options.type) {
      transformedOptions.type = `${options.type}-light`;
    }

    this.onCreate.emit(options);
  }

  createAlert(options: ToastOptions) {
    this.create({ ...options, type: ToastTypes.ALERT_LIGHT });
  }

  createSuccess(options: ToastOptions) {
    this.create({ ...options, type: ToastTypes.SUCCESS_LIGHT });
  }

  createWarning(options: ToastOptions) {
    this.create({ ...options, type: ToastTypes.WARNING_LIGHT });
  }

  createInfo(options: ToastOptions) {
    this.create({ ...options, type: ToastTypes.INFO_LIGHT });
  }
}
