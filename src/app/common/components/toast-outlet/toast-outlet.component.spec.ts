import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToastService } from './services/toast-outlet.service';
import { ToastOutletComponent } from './toast-outlet.component';

describe('ToastOutletComponent', () => {
  let component: ToastOutletComponent;
  let fixture: ComponentFixture<ToastOutletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ToastOutletComponent],
      providers: [ToastService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToastOutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
