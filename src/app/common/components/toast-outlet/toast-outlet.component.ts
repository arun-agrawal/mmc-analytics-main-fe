import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

import { ToastOptions } from '../../models/toast-option/toast-option.models';
import { ToastService } from './services/toast-outlet.service';

@Component({
  selector: 'mercer-app-toast-outlet',
  templateUrl: './toast-outlet.component.html',
  styleUrls: ['./toast-outlet.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToastOutletComponent implements OnInit, OnDestroy {
  /** @internal */
  _appToastOptions$ = new BehaviorSubject<ToastOptions>(null);

  @ViewChild('appToast', { static: false }) appToast;

  private _subscription = new Subscription();

  constructor(private readonly _toastService: ToastService) {}

  ngOnInit() {
    this._subscription.add(
      this._toastService.onCreate.subscribe((options: ToastOptions) => {
        this._appToastOptions$.next(options);
        if (this.appToast) {
          this.appToast.open();
        }
      })
    );
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
