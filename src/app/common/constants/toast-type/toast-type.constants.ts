export enum ToastTypes {
  SUCCESS_LIGHT = 'success-light',
  ALERT_LIGHT = 'alert-light',
  INFO_LIGHT = 'info-light',
  WARNING_LIGHT = 'warning-light'
}
