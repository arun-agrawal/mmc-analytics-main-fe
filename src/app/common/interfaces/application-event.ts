export interface ApplicationEvent {
    _id?: string;
    appid: string;
    pageurl: string;
    name: string;
    type: string;
    category: string;
    element: any;
}