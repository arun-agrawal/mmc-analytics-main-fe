export interface Application {
    _id?: string;
    name: string;
    description: string;
    url: string;
    owners: string[];
}