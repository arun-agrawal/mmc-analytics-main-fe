import { ToastTypes } from '../../constants/toast-type/toast-type.constants';

export interface ToastOptions {
  type?: ToastTypes;
  title?: string;
  autoDismiss?: boolean;
  message: string;
}
