import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RouteService } from './route/route.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [RouteService]
})
export class ServicesModule {}
