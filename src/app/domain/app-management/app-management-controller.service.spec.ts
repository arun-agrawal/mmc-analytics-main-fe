import { TestBed } from '@angular/core/testing';

import { AppManagementControllerService } from './app-management-controller.service';

describe('AppManagementControllerService', () => {
  let service: AppManagementControllerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppManagementControllerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
