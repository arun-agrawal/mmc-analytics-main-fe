import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { shareReplay, take } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastService } from '../../features/shared/services/toast.service';
import { getBackendUrl } from '../../common/utils/environment/environment';

import { Application, ApplicationEvent } from '../../common/interfaces';
import {
  loadAllApps, loadApp, registerApp, updateApp, deleteApp,
  loadAppManagements, deleteAppEvent, updateAppEvent,
  loadAppEvent, addAppEvent, loadAppAllEvents,
  loadEventsLogGroupBy_EventType_YYYYMM

} from './app-management.actions';
import {
  selectAllApps, selectApp, selectAppManagementState, selectAppEvent, selectAppEvents,
  selectAppEventLogsGroupByEventTypeOnYYYYMM
} from './app-management.selectors';


@Injectable({
  providedIn: 'root'
})
export class AppManagementControllerService {
  _currentApp$ = this._store.select(selectApp);
  _currentEvent$ = this._store.select(selectAppEvent);
  _currentAppEvents$ = this._store.select(selectAppEvents);

  _eventLogsGroupByEventTypeOnYYYYMM$ = this._store.select(selectAppEventLogsGroupByEventTypeOnYYYYMM);
  _allApps$ = this._store.select(selectAllApps);

  constructor(private _store: Store<any>) {

  }

  loadAllApplications() {
    this._store.dispatch(loadAllApps());
  }

  loadApplication(appId) {
    this._store.dispatch(loadApp({ id: appId }));
  }

  registerApplication(app: Application) {
    this._store.dispatch(registerApp({ data: app }));
  }

  updateApplication(app: Application) {
    this._store.dispatch(updateApp({ id: app._id, data: app }));
  }

  deleteApplication(app: Application) {
    this._store.dispatch(deleteApp({ id: app._id }));
  }

  loadApplicationEvents(appId) {
    this._store.dispatch(loadAppAllEvents({ id: appId }));
  }

  loadApplicationEvent(appEventId) {
    this._store.dispatch(loadAppEvent({ id: appEventId }));
  }

  addApplicationEvent(appEvent: ApplicationEvent) {
    this._store.dispatch(addAppEvent({ data: appEvent }));
  }

  updateApplicationEvent(appEvent: ApplicationEvent) {
    this._store.dispatch(updateAppEvent({ id: appEvent._id, data: appEvent }));
  }

  deleteApplicationEvent(appEvent: ApplicationEvent) {
    this._store.dispatch(deleteAppEvent({ id: appEvent._id }));
  }


  loadEventLogsGroupBy_EventType_YYYYMM(appId) {
    this._store.dispatch(loadEventsLogGroupBy_EventType_YYYYMM({ id: appId }));
  }

}
