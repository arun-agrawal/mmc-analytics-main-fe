import { createAction, props } from '@ngrx/store';
import { Application, ApplicationEvent } from '../../common/interfaces';

export const loadAppManagements = createAction('[AppManagement] Load AppManagements');

export const loadApp = createAction('[AppManagement] Load App', props<{ id: string; }>());
export const loadAppSuccess = createAction(
  '[AppManagement] Load App Success',
  props<{ data: Application }>(),
);
export const loadAppFailure = createAction('[AppManagement] Load App Failure', props<{ error: any }>());


export const registerApp = createAction(
  '[App] Register App',
  props<{ data: any; }>(),
);
export const registerAppSuccess = createAction(
  '[App] Register App Success',
  props<{ data: any }>(),
);

export const registerAppFailure = createAction(
  '[App] Register App Failure',
  props<{ error: any }>(),
);

export const updateApp = createAction(
  '[App] Update App',
  props<{ id: string; data: any; }>(),
);

export const updateAppSuccess = createAction(
  '[App] Update App Success',
  props<{ id: string; data: any }>(),
);

export const updateAppFailure = createAction(
  '[App] Update App Failure',
  props<{ error: any }>(),
);

export const deleteApp = createAction(
  '[App] Delete App',
  props<{ id: string; }>(),
);

export const deleteAppSuccess = createAction(
  '[App] Delete App Success',
  props<{ id: string }>(),
);

export const deleteAppFailure = createAction(
  '[App] Delete App Failure',
  props<{ error: any }>(),
);



export const loadAllApps = createAction(
  '[AppManagement] Load All Apps',
);

export const loadAllAppsSuccess = createAction(
  '[AppManagement] Load All Apps Success',
  props<{ data: Application[] }>(),
);

export const loadAllAppsFailure = createAction(
  '[AppManagement] Load All Apps Failure',
  props<{ error: any }>(),
);

//Application Event Start

export const loadAppEvent = createAction('[AppManagement] Load App Event', props<{ id: string; }>());
export const loadAppEventSuccess = createAction(
  '[AppManagement] Load App Event Success',
  props<{ data: ApplicationEvent }>(),
);
export const loadAppEventFailure = createAction('[AppManagement] Load App Event Failure', props<{ error: any }>());


export const addAppEvent = createAction(
  '[App] Add App Event',
  props<{ data: any; }>(),
);
export const addAppEventSuccess = createAction(
  '[App] Add App Success',
  props<{ data: any }>(),
);
export const addAppEventFailure = createAction(
  '[App] Add App Failure',
  props<{ error: any }>(),
);

export const updateAppEvent = createAction(
  '[App] Update App Event',
  props<{ id: string; data: any; }>(),
);

export const updateAppEventSuccess = createAction(
  '[App] Update App Event Success',
  props<{ id: string; data: any }>(),
);

export const updateAppEventFailure = createAction(
  '[App] Update App Event Failure',
  props<{ error: any }>(),
);

export const deleteAppEvent = createAction(
  '[App] Delete App Event',
  props<{ id: string; }>(),
);

export const deleteAppEventSuccess = createAction(
  '[App] Delete App Event Success',
  props<{ id: string }>(),
);

export const deleteAppEventFailure = createAction(
  '[App] Delete App Event Failure',
  props<{ error: any }>(),
);



export const loadAppAllEvents = createAction(
  '[AppManagement] Load App All Events',
  props<{ id: string; }>()
);

export const loadAppAllEventsSuccess = createAction(
  '[AppManagement] Load Apps All Events Success',
  props<{ data: ApplicationEvent[] }>(),
);

export const loadAppAllEventsFailure = createAction(
  '[AppManagement] Load Apps All Events Failure',
  props<{ error: any }>(),
);

export const loadEventsLogGroupBy_EventType_YYYYMM = createAction(
  '[AppManagement] Load AppEventLogs GroupBy EventType YYYYMM',
  props<{ id: string; }>()
);

export const loadEventsLogGroupBy_EventType_YYYYMM_Success = createAction(
  '[AppManagement] Load AppEventLogs GroupBy EventType YYYYMM Success',
  props<{ data: any }>(),
);

export const loadEventsLogGroupBy_EventType_YYYYMM_Failure = createAction(
  '[AppManagement] Load AppEventLogs GroupBy EventType YYYYMM Failure',
  props<{ error: any }>(),
);
