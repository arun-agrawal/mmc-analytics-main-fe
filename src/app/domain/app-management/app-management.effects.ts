import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { from, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { ToastService } from '../../features/shared/services/toast.service';
import { getBackendUrl } from '../../common/utils/environment/environment';
import * as AppManagementActions from './app-management.actions';
import { Application, ApplicationEvent } from '../../common/interfaces';


@Injectable()
export class AppManagementEffects {


  loadAllApps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppManagementActions.loadAllApps),
      switchMap(() => this._loadApplications().pipe(
        map((data: Application[]) => AppManagementActions.loadAllAppsSuccess({ data })),
        catchError(error => of(AppManagementActions.loadAllAppsFailure({ error }))),
      ))
    );
  });

  loadApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppManagementActions.loadApp),
      switchMap(({ id }) => this._loadApplication(id).pipe(
        map((data: Application) => AppManagementActions.loadAppSuccess({ data })),
        catchError(error => of(AppManagementActions.loadAppFailure({ error }))),
      ))
    );
  });

  registerApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppManagementActions.registerApp),
      switchMap(({ data: payload }) => this._registerApplication(payload).pipe(
        switchMap((data: any) => {
          this._toastService.createSuccess({ message: 'Application added successfully' });
          return of(
            AppManagementActions.loadAllApps(),
            AppManagementActions.registerAppSuccess({ data }))
        }),
        catchError(error => {
          this._toastService.createAlert({ message: 'Add application Error: ' + error.error.errmsg });
          return of(AppManagementActions.registerAppFailure({ error }))
        }),
      )),
    );
  });

  updateApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppManagementActions.updateApp),
      switchMap(({ data: payload, id: id }) => this._updateApplication(id, payload).pipe(
          switchMap((data: any) => {
            this._toastService.createSuccess({ message: 'Application details updated successfully' });
            return of(
              AppManagementActions.loadAllApps(),
              AppManagementActions.updateAppSuccess({ id, data }))
          }),
        catchError(error => {
          this._toastService.createAlert({ message: 'Application details update Error: ' + error.error.errmsg });
          return of(AppManagementActions.updateAppFailure({ error }))
        }),
      )),
    );
  });

  deleteApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppManagementActions.deleteApp),
      switchMap(({ id }) => this._deleteApplication(id).pipe(
          switchMap((data: any) => {
            this._toastService.createSuccess({ message: 'Application removed successfully' });
            return of(
              AppManagementActions.loadAllApps(),
              AppManagementActions.deleteAppSuccess({ id }))
          }),
        catchError(error => {
          this._toastService.createAlert({ message: 'Delete Application Error: ' + error.error.errmsg });
          return of(AppManagementActions.deleteAppFailure({ error }))
        }),
      )),
    );
  });

  //Application Event

  loadAppAllEvents$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppManagementActions.loadAppAllEvents),
      switchMap(({ id }) => this._loadApplicationEvents(id).pipe(
        map((data: ApplicationEvent[]) => AppManagementActions.loadAppAllEventsSuccess({ data })),
        catchError(error => of(AppManagementActions.loadAppAllEventsFailure({ error }))),
      ))
    );
  });

  loadAppEvent$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppManagementActions.loadAppEvent),
      switchMap(({ id }) => this._loadApplicationEvent(id).pipe(
        map((data: ApplicationEvent) => AppManagementActions.loadAppEventSuccess({ data })),
        catchError(error => of(AppManagementActions.loadAppEventFailure({ error }))),
      ))
    );
  });

  addAppEvent$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppManagementActions.addAppEvent),
      switchMap(({ data: payload }) => this._addApplicationEvent(payload).pipe(
        switchMap((result: any) => {
          this._toastService.createSuccess({ message: 'Application Event added successfully' });
          return of(
            AppManagementActions.loadAppAllEvents({id: result.data.appid}),
            AppManagementActions.addAppEventSuccess(result.data))
        }),
        catchError(error => {
          this._toastService.createAlert({ message: 'Add application event error: ' + error.error.errmsg });
          return of(AppManagementActions.addAppEventFailure({ error }))
        }),
      )),
    );
  });

  updateAppEvent$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppManagementActions.updateAppEvent),
      switchMap(({ data: payload, id: id }) => this._updateApplicationEvent(id, payload).pipe(
          switchMap((result: any) => {
            this._toastService.createSuccess({ message: 'Application event updated successfully' });
            console.log('update =>', result.data.appid, result.data);
            return of(
              AppManagementActions.loadAppAllEvents({id: result.data.appid}),
              AppManagementActions.updateAppEventSuccess({ id, data: result.data }))
          }),
        catchError(error => {
          this._toastService.createAlert({ message: 'Application event update Error: ' + error.error.errmsg });
          return of(AppManagementActions.updateAppEventFailure({ error }))
        }),
      )),
    );
  });

  deleteAppEvent$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppManagementActions.deleteAppEvent),
      switchMap(({ id }) => this._deleteApplicationEvent(id).pipe(
          switchMap((result: any) => {
            this._toastService.createSuccess({ message: 'Application event removed successfully' });
            return of(
              AppManagementActions.loadAppAllEvents({id: result.data.appid}),
              AppManagementActions.deleteAppEventSuccess({ id }))
          }),
        catchError(error => {
          this._toastService.createAlert({ message: 'Delete application event error: ' + error.error.errmsg });
          return of(AppManagementActions.deleteAppEventFailure({ error }))
        }),
      )),
    );
  });

  loadAppEventsGroupByEventTypeOnYYYYMM$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppManagementActions.loadEventsLogGroupBy_EventType_YYYYMM),
      switchMap(({ id }) => this._loadEventsLogGroupBy_EventType_YYYYMM(id).pipe(
        map((data: ApplicationEvent) => AppManagementActions.loadEventsLogGroupBy_EventType_YYYYMM_Success({ data })),
        catchError(error => of(AppManagementActions.loadEventsLogGroupBy_EventType_YYYYMM_Failure({ error }))),
      ))
    );
  });

  constructor(private actions$: Actions,
    private _httpClient: HttpClient,
    private _toastService: ToastService) { }

  private _loadApplications() {
    return this._httpClient.get(`${getBackendUrl()}/clientapp/`);
  }

  private _loadApplication(appId) {
    return this._httpClient.get(`${getBackendUrl()}/clientapp/${appId}`);
  }

  private _registerApplication(application) {
    return this._httpClient.post(`${getBackendUrl()}/clientapp`, application);
  }

  private _updateApplication(appId, application) {
    return this._httpClient.put(`${getBackendUrl()}/clientapp/${appId}`, application);
  }

  private _deleteApplication(appId) {
    return this._httpClient.delete(`${getBackendUrl()}/clientapp/${appId}`);
  }

  private _loadApplicationEvents(appId) {
    return this._httpClient.get(`${getBackendUrl()}/clientappevent/${appId}`);
  }

  private _loadApplicationEvent(appEventId) {
    return this._httpClient.get(`${getBackendUrl()}/clientappevent/event/${appEventId}`);
  }

  private _addApplicationEvent(appEvent) {
    return this._httpClient.post(`${getBackendUrl()}/clientappevent`, appEvent);
  }

  private _updateApplicationEvent(appEventId, appEvent) {
    return this._httpClient.put(`${getBackendUrl()}/clientappevent/${appEventId}`, appEvent);
  }

  private _deleteApplicationEvent(appEventId) {
    return this._httpClient.delete(`${getBackendUrl()}/clientappevent/event/${appEventId}`);
  }

  private _loadEventsLogGroupBy_EventType_YYYYMM(appId) {
    return this._httpClient.get(`${getBackendUrl()}/event/groupby/user-events?appid=${appId}`);
  }

}
