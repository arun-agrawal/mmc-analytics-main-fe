import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromAppManagement from './app-management.reducer';
import { EffectsModule } from '@ngrx/effects';
import { AppManagementEffects } from './app-management.effects';
//import { AppManagementComponent } from './components/app-management/app-management.component';



@NgModule({
  declarations: [
    //AppManagementComponent
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromAppManagement.appManagementFeatureKey, fromAppManagement.reducer),
    EffectsModule.forFeature([AppManagementEffects])
  ]
})
export class AppManagementModule { }
