import { Action, createReducer, on } from '@ngrx/store';
import { Application, ApplicationEvent } from '../../common/interfaces';

import * as AppManagementActions from './app-management.actions';

export const appManagementFeatureKey = 'appManagement';


export interface State {
  loadedApplication?: Application;
  loadedApplicationEvent?: ApplicationEvent;

  applications?: Application[];
  applicationEvents?: ApplicationEvent[];

  eventsByEventTypeOnYYYYMM?: any[];

}

export const initialState: State = {
};

const appManagementReducer = createReducer(
  initialState,

  on(AppManagementActions.loadAppManagements, state => state),

  on(AppManagementActions.loadAppSuccess, (state, action) => ({ ...state, loadedApplication: action.data })),

  on(AppManagementActions.loadAllAppsSuccess, (state, action) => ({ ...state, applications: action.data })),

  on(AppManagementActions.loadAppEventSuccess, (state, action) => ({ ...state, loadedApplicationEvent: action.data })),

  on(AppManagementActions.loadAppAllEventsSuccess, (state, action) => ({ ...state, applicationEvents: action.data })),

  on(AppManagementActions.loadEventsLogGroupBy_EventType_YYYYMM_Success, (state, action) => ({ ...state, eventsByEventTypeOnYYYYMM: action.data })),
);

export function reducer(state: State | undefined, action: Action) {
  return appManagementReducer(state, action);
}
