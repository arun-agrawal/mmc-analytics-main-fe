import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromAppManagement from './app-management.reducer';


export const selectAppManagementState = createFeatureSelector<fromAppManagement.State>(
  fromAppManagement.appManagementFeatureKey,
);


export const selectApp = createSelector(selectAppManagementState, (state: fromAppManagement.State) =>
  state.loadedApplication
);

export const selectAllApps = createSelector(
  selectAppManagementState,
  (state: fromAppManagement.State) => state.applications,
);

export const selectAppEvent = createSelector(selectAppManagementState, (state: fromAppManagement.State) =>
  state.loadedApplicationEvent
);

export const selectAppEvents = createSelector(
  selectAppManagementState,
  (state: fromAppManagement.State) => state.applicationEvents,
);

export const selectAppEventLogsGroupByEventTypeOnYYYYMM = createSelector(
  selectAppManagementState,
  (state: fromAppManagement.State) => state.eventsByEventTypeOnYYYYMM,
);
