import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AppManagementModule } from './app-management/app-management.module';

const DOMAIN_MODULE = [
  CommonModule,
  AppManagementModule
];

@NgModule({
  declarations: [],
  imports: DOMAIN_MODULE,
  exports: DOMAIN_MODULE
 
})
export class DomainModule { }
