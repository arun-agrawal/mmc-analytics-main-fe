import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  LOGIN_FINISH,
  LOGOUT_FINISH,
  LoginFinishAction,
  LogoutFinishAction,
  SESSION_TIMEOUT_CUSTOM_LOGOUT_HOOK,
  SessionTimeoutCustomLogoutAction,
  MFA_CUSTOM_LOCKOUT_HOOK,
  MfaCustomLockoutHookAction,
} from 'ngpd-merceros-authentication-fe-components';
import { map, tap } from 'rxjs/operators';

import { AuthHookService } from './auth-hook.service';


@Injectable()
export class AuthHookEffects {

  @Effect({dispatch: false})
  authLoginFinishHook$ = this._actions$.pipe(
    ofType<LoginFinishAction>(LOGIN_FINISH),
    tap(() => this._authHookService.postLogin())
  );

  @Effect({dispatch: false})
  authLogoutFinishHook$ = this._actions$.pipe(
    ofType<LogoutFinishAction>(LOGOUT_FINISH),
    tap(() => this._authHookService.postLogout())
  );

  @Effect({dispatch: false})
  authSessionTimeoutCustomHook$ = this._actions$.pipe(
    ofType<SessionTimeoutCustomLogoutAction>(SESSION_TIMEOUT_CUSTOM_LOGOUT_HOOK),
    tap(() => console.log(`need to set common config 'customSessionTimeoutLogout' to true and need to do reroute here`))
  );

  @Effect({dispatch: false})
  authMfaCustomLockoutHook$ = this._actions$.pipe(
    ofType<MfaCustomLockoutHookAction>(MFA_CUSTOM_LOCKOUT_HOOK),
    map(res => res.payload),
    tap(incidentData => this._authHookService.customMfaLockout(incidentData))
  );

  constructor(
    private _actions$: Actions,
    private _authHookService: AuthHookService
  ) { }
}
