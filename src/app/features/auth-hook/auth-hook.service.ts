import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { ServerErrorAction, ServerSuccessAction } from '../shared/state/toast/toast.actions';

@Injectable()
export class AuthHookService {

  constructor(private _store: Store<any>) { }

  postLogin() {
    this._store.dispatch(new ServerSuccessAction({
      title: 'Log In!',
      message: 'You have logged in successfully!',
    }));
  }

  postLogout() {
    this._store.dispatch(new ServerSuccessAction({
      title: 'Log Out!',
      message: 'You have logged out successfully!',
    }));
  }

  customMfaLockout(incidentData: any) {
    console.log(incidentData);
    this._store.dispatch(new ServerErrorAction('You have been locked out, please contact support'));
  }
}
