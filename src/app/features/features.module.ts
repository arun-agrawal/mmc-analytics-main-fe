import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { AuthenticationModule } from 'ngpd-merceros-authentication-fe-components';

import { environment } from '../../environments/environment';
import { englishLabels } from '../language-pack/english';
import { AuthHookEffects } from './auth-hook/auth-hook.effects';
import { AuthHookService } from './auth-hook/auth-hook.service';
import { SharedModule } from './shared/shared.module';
import {
  SessionTimeoutWarningExampleComponent,
} from './shared/components/session-timeout-warning/session-timeout-warning-example.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    EffectsModule.forFeature([AuthHookEffects]),
    AuthenticationModule.forRoot({
      common: {
        appName: environment.appName,
        apiUrl: environment.API,
        displayName: environment.displayName,
        loginSuccessRoute: environment.loginSuccessRoute,
        jwtTokenExpiresIn: environment.jwtTokenExpiresIn,
        refreshTokenExpiresIn: environment.refreshTokenExpiresIn,
        disableSessionTimeout: environment.sessionTimeoutDisabled,
        sessionTimeoutWarningTime: environment.sessionTimeoutWarningTime,
        userProfileReturnRoute: environment.authUserProfileReturnRoute,
        labels: englishLabels,
        // customSessionTimeoutWarningTemplate: SessionTimeoutWarningExampleComponent,
        // defaultSuccessSingleLogoutBehavior: false,
        devMode: environment.authDevMode,
      },
      msso: {
        federatedOnly: environment.mssoFederatedOnly,
        mssoSupportEmail: environment.mssoSupportEmail,
        bypassMsso: environment.mssoDisabled,
        federatedLoginUrl: `${environment.API}/msso/login/federated`,
        rpid: environment.rpid,
        mssoFailureUrl: environment.mssoFailureUrl,
        mssoLoginUrl: environment.mssoLoginPost,
        homeRealmUrl: `${environment.API}/msso/homerealm`,
        displayRegistrationTermsAndCondition: true,
        skipPrefilledRegistrationScreens: true,
      },
      mfa: {
        mfaSupportEmail: environment.mfaSupportEmail,
        bypassMfa: environment.mfaDisabled,
        defaultLockoutFlow: true,
        // locale: environment.startingMfaLocale,
      }
    }),
  ],
  declarations: [],
  providers: [AuthHookService],
  exports: [SharedModule],
})
export class FeaturesModule { }
