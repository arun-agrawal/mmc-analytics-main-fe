import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { AuthFacadeService } from 'ngpd-merceros-authentication-fe-components';
import { BehaviorSubject } from 'rxjs';


@Component({
  selector: 'mercer-app-auth-session-timeout-warning-example',
  templateUrl: './session-timeout-warning-example.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionTimeoutWarningExampleComponent {
  @Input() timer: BehaviorSubject<number>;

  constructor(private _authFacade: AuthFacadeService) { }

  logout() {
    this._authFacade.logout();
  }

  continue() {
    this._authFacade.resetSessionTimeout();
  }

  minuteConvert(minutes: number) {
    return Math.floor(minutes);
  }
}
