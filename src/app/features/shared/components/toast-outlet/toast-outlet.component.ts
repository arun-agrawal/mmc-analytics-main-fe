import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';

import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'mercer-app-auth-toast-outlet',
  templateUrl: 'toast-outlet.component.html',
  styleUrls: ['toast-outlet.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToastOutletComponent implements OnInit, OnDestroy {
  @ViewChild('appToast') appToast;

  appToastOptions: any = {};

  private _toastSub: Subscription;

  constructor(public toastCtrl: ToastService) { }

  ngOnInit() {
    this._toastSub = this.toastCtrl.onCreate.subscribe((options) => {
      this.appToastOptions = options;
      if (this.appToast) {
        this.appToast.open();
      }
    });
  }

  ngOnDestroy() {
    this._toastSub.unsubscribe();
  }
}
