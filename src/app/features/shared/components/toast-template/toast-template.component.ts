import { Component, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'toast-template',
  templateUrl: './toast-template.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToastTemplateComponent {
  title: string;
  id: string;
  message: string;
  showButton: boolean;
  showCloseButton: boolean;
  buttonTitle: string;
  buttonCallback: () => void;
}
