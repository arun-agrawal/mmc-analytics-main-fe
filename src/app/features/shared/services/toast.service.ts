import { Injectable, EventEmitter } from '@angular/core';

export interface ToastOptions {
  type?: 'success-light' | 'alert-light' | 'info-light' | 'warning-light';
  title?: string;
  autoDismiss?: boolean;
  message: string;
}

@Injectable()
export class ToastService {
  /**
   * Emits on creation event with options
   */
  onCreate = new EventEmitter();

  /**
   * Creates and shows a page level toast (defaults to info type)
   *
   */
  create(options: ToastOptions) {
    if (!options) {
      return;
    }
    const transformedOptions = { ...options } as any;

    if (options.type) {
      transformedOptions.type = `${options.type}-light`;
    }

    this.onCreate.emit(options);
  }

  /**
   * Convenience function for creating an alert
   *
   */
  createAlert(options: ToastOptions) {
    this.create({ ...options, type: 'alert-light' });
  }

  /**
   * Convenience function for creating a success toast
   */
  createSuccess(options: ToastOptions) {
    this.create({ ...options, type: 'success-light' });
  }

  /**
   * Convenience function for creating a warning toast
   */
  createWarning(options: ToastOptions) {
    this.create({ ...options, type: 'warning-light' });
  }
}
