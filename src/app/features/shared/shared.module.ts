import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { MercerOSModule } from 'merceros-ui-components';

import { SessionTimeoutWarningExampleComponent } from './components/session-timeout-warning/session-timeout-warning-example.component';
import { ToastTemplateComponent } from './components/toast-template/toast-template.component';
import { ToastOutletComponent } from './components/toast-outlet/toast-outlet.component';
import { ToastService } from './services/toast.service';
import { ErrorEffects } from './state/error/error.effects';
import { ToastEffects } from './state/toast/toast.effects';

const COMPONENTS = [
  SessionTimeoutWarningExampleComponent,
  ToastOutletComponent,
  ToastTemplateComponent,
];

@NgModule({
  declarations: COMPONENTS,
  imports: [
    CommonModule,
    MercerOSModule,
    EffectsModule.forFeature([ErrorEffects, ToastEffects]),
  ],
  providers: [ToastService],
  exports: [...COMPONENTS],
  entryComponents: [SessionTimeoutWarningExampleComponent, ToastTemplateComponent],
})
export class SharedModule {}
