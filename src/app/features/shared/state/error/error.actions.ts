import { Action } from '@ngrx/store';

export const ActionTypes = {
  SERVER_ERROR: '[error] Server Error',
};

export class ServerErrorAction implements Action {
  public type = ActionTypes.SERVER_ERROR;
  constructor(public payload: string) { }
}

export type Actions = ServerErrorAction;
