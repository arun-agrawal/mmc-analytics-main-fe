import { Actions, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { ServerErrorAction, ActionTypes } from './error.actions';
import { ToastService } from '../../services/toast.service';

@Injectable()
export class ErrorEffects {

  @Effect({ dispatch: false })
  serverError$ = this._actions$.pipe(
    ofType<ServerErrorAction>(ActionTypes.SERVER_ERROR),
    map(action => action.payload),
    tap((errorMessage) => {
      this._toastService.createAlert({ message: errorMessage });
    })
  );

  constructor(private _actions$: Actions, private _toastService: ToastService) { }
}
