import { Action } from '@ngrx/store';

export const ActionTypes = {
  SERVER_ERROR: '[toast] Server Error',
  SERVER_SUCCESS: '[toast] Server Success',
  SERVER_MESSAGE: '[toast] Server Message',
};

export class ServerErrorAction implements Action {
  public type = ActionTypes.SERVER_ERROR;
  constructor(public payload: string) { }
}

export class ServerSuccessAction implements Action {
  public type = ActionTypes.SERVER_SUCCESS;
  constructor(public payload: any) { }
}

export class ServerMessageAction implements Action {
  public type = ActionTypes.SERVER_MESSAGE;
  constructor(public payload: string) { }
}

export type Actions = ServerErrorAction;
