import { Actions, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { ServerErrorAction, ActionTypes, ServerMessageAction, ServerSuccessAction } from './toast.actions';
import { ToastService } from '../../services/toast.service';

@Injectable()
export class ToastEffects {

  @Effect({ dispatch: false })
  serverError$ = this._actions$.pipe(
    ofType<ServerErrorAction>(ActionTypes.SERVER_ERROR),
    map(action => action.payload),
    tap((errorMessage) => {
      this._toastService.createAlert({ message: errorMessage });
    })
  );

  @Effect({ dispatch: false })
  serverSuccess$ = this._actions$.pipe(
    ofType<ServerSuccessAction>(ActionTypes.SERVER_SUCCESS),
    map(action => action.payload),
    tap((payload) => {
      this._toastService.create({
        type: 'success-light',
        title: payload.title,
        message: payload.message,
      });
    })
  );

  @Effect({ dispatch: false })
  serverMessage$ = this._actions$.pipe(
    ofType<ServerMessageAction>(ActionTypes.SERVER_MESSAGE),
    map(action => action.payload),
    tap((message) => {
      this._toastService.create({
        type: 'info-light',
        message,
      });
    })
  );

  constructor(private _actions$: Actions, private _toastService: ToastService) {}
}
