export const englishLabels = {
  COMMON_LABELS: {
    TOP_TITLE: `Authentication Package MSSO`,
    EMAIL_LABEL: `Username`,
    SINGLE_LOGOUT_SUCCESS_MESSAGE: `Successfully Single Logged Out!`,
  },
  LOGIN: {
    REGISTRATION_LINK: `Click here to register`,
  },
};
