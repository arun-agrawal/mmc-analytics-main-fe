import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddClientAppEventComponent } from './add-clientapp-event.component';

describe('AddClientAppEventComponent', () => {
  let component: AddClientAppEventComponent;
  let fixture: ComponentFixture<AddClientAppEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddClientAppEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddClientAppEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
