import {
  Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
  Input, Output, ViewChild, EventEmitter, ElementRef, ChangeDetectorRef
} from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { AppManagementControllerService } from '../../../domain/app-management/app-management-controller.service';
import { Application, ApplicationEvent } from '../../../common/interfaces';

@Component({
  selector: 'mmcanalytics-add-clientapp-event',
  templateUrl: './add-clientapp-event.component.html',
  styleUrls: ['./add-clientapp-event.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default
})
export class AddClientAppEventComponent implements OnInit {
  @Output() onClose = new EventEmitter();
  @Output() onValidationError = new EventEmitter();

  @Input('application') application: Application;

  @ViewChild('inputForm') inputForm: ElementRef;
  @ViewChild('eventElementWindow') eventElementWindow: any;

  public addApplicationEventForm: FormGroup;
  public clientAppUrl: any = '';
  public capturedEvent: any = { pageurl: '', type: '', element: {} }

  constructor(private _formBuilder: FormBuilder,
    private _changeDetectorRef: ChangeDetectorRef,
    private sanitizer: DomSanitizer,
    private _appManagementControllerService: AppManagementControllerService) {
    this.addApplicationEventForm = this._formBuilder.group({
      appEventPageURL: new FormControl(null),
      appEventName: new FormControl(null),
      appEventType: new FormControl(null),
      appEventCategory: new FormControl(null),
      appEventElement: new FormControl(null),
    });
  }

  ngOnInit(): void {
  }

  submit(skipWarning: boolean = false) {
    let appEvent: ApplicationEvent = {
      appid: this.trimValue(this.application._id),
      pageurl: this.trimValue(this.addApplicationEventForm.value?.appEventPageURL),
      name: this.trimValue(this.addApplicationEventForm.value?.appEventName),
      type: this.trimValue(this.addApplicationEventForm.value?.appEventType),
      category: this.trimValue(this.addApplicationEventForm.value?.appEventCategory), 
      element: (this.addApplicationEventForm.value?.appEventElement !== '') ? JSON.parse(this.addApplicationEventForm.value?.appEventElement) : {},
    };

    this._appManagementControllerService.addApplicationEvent(appEvent);
    this.addApplicationEventForm.reset();
    this.onClose.emit();
  }

  trimValue(value) {
    return (value) ? value.trim() : value;
  }

  trimURL(url) {
    if (url && url.length > 0 && url.toString().lastIndexOf("/") === (url.length - 1)) {
      url = url.toString().substr(0, url.length - 1);
    }

    return url;
  }

  captureEventElement() {
    this.clientAppUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.application?.url);
    this.eventElementWindow.open();

    this.capturedEvent.pageurl = '';
    this.capturedEvent.type = '';
    this.capturedEvent.element = {};
    this._changeDetectorRef.detectChanges();

    setTimeout(() => {
      // get reference to window inside the iframe
      //var wn = document.getElementById('clientAppIframe')..contentWindow;
      let win: any = window.frames;
      // postMessage arguments: data to send, target origin
      win.clientAppIframe?.postMessage({ capturetodefine: true }, this.application.url);
      window.addEventListener("message", this.showCapturedEvent.bind(this), false);
    }, 3000);
  }



  showCapturedEvent(event) {
    console.log('In Parent =>', this.application.url, event.origin, event.data);

    if (!event || (this.trimURL(event.origin) != this.trimURL(this.application.url))) {
      // something from an unknown domain, let's ignore it
      return;
    }

    this.capturedEvent.pageurl = event.data.pageurl;
    this.capturedEvent.type = event.data.event?.type;
    this.capturedEvent.element = event.data.event?.element;
    this._changeDetectorRef.detectChanges();
  }

  setCaptureEventElement() {
    this.addApplicationEventForm.patchValue({
      appEventPageURL: this.capturedEvent.pageurl,
      appEventType: this.capturedEvent.type,
      appEventElement: JSON.stringify(this.capturedEvent.element)
    });
    this.eventElementWindow.close();
  }

}
