import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientAppEventComponent } from './client-app-event.component';


const routes: Routes = [{ path: '', component: ClientAppEventComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientAppEventRoutingModule { }
