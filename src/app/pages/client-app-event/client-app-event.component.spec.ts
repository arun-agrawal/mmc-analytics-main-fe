import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAppEventComponent } from './client-app-event.component';

describe('ClientAppEventComponent', () => {
  let component: ClientAppEventComponent;
  let fixture: ComponentFixture<ClientAppEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientAppEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAppEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
