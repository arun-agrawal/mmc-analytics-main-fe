import {
  Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
  ViewChild, Renderer2, ElementRef, ChangeDetectorRef
} from '@angular/core';
import { Router } from '@angular/router';
import { MosModalComponent, PaginationInfo } from 'merceros-ui-components';
import { AddClientAppEventComponent } from './add-clientapp-event/add-clientapp-event.component';
import { EditClientAppEventComponent } from './edit-clientapp-event/edit-clientapp-event.component';

import { AppManagementControllerService } from '../../domain/app-management/app-management-controller.service';
import { environment } from '../../../environments/environment';
import { Application, ApplicationEvent } from '../../common/interfaces';
import { cloneDeep } from 'lodash-es';


export class AppInfo { title: string = ""; type: string = ""; headertext: string = ""; content: string[] = []; footertext: string = "" }

@Component({
  templateUrl: './client-app-event.component.html',
  styleUrls: ['./client-app-event.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClientAppEventComponent implements OnInit {

  public applications: Application[] = [];
  public applicationEvents: ApplicationEvent[] = [];

  public selectedApp: Application;
  public selectedAppId: String = '';
  public selectedAppEvent: ApplicationEvent;

  public mode: string = 'add';

  @ViewChild('addEditClientAppEventModal') addEditClientAppEventModal: MosModalComponent;
  @ViewChild('eventCaptureModal') eventCaptureModal: MosModalComponent;
  @ViewChild('infoDiv', { static: false }) infoDiv: ElementRef;

  @ViewChild('addClientAppEventForm') addClientAppEventForm: AddClientAppEventComponent;
  @ViewChild('editClientAppEventForm') editClientAppEventForm: EditClientAppEventComponent;

  constructor(private renderer: Renderer2,
    private _changeDetectorRef: ChangeDetectorRef,
    private _router: Router,
    private _appManagementControllerService: AppManagementControllerService) {

    this._appManagementControllerService.loadAllApplications();
    this._appManagementControllerService._allApps$.subscribe((apps: any) => {
      this.applications = (apps && apps.success) ? apps.data : [];
      setTimeout(() => { this._changeDetectorRef.detectChanges() }, 100);
    });
  }

  ngOnInit(): void {
    //console.log('fetch app id =>', localStorage.getItem('currentClientApp'));
    setTimeout(() => {
      this.selectedAppId = localStorage.getItem('currentClientApp');
      this._changeDetectorRef.detectChanges();
      this.onApplicationSelect(null);
      }, 100);
  }

  onApplicationSelect(event) {
    console.log("on app select =>", event?.target?.value, this.selectedAppId)
    this.selectedApp = this.applications.find(app => app._id === this.selectedAppId);
    localStorage.setItem('currentClientApp',this.selectedApp?._id);

    this._appManagementControllerService.loadApplicationEvents(this.selectedApp?._id);
    this._appManagementControllerService._currentAppEvents$.subscribe((appEvents: any) => {
      this.applicationEvents = (appEvents && appEvents.success) ? appEvents.data : [];
      console.log("app events =>", appEvents, this.selectedApp)
      this._changeDetectorRef.detectChanges();
    });
  }

  openModal(_mode, _appEvent) {
    this.mode = _mode;
    this.selectedAppEvent = (_appEvent) ? cloneDeep(_appEvent) : null;
    this.addEditClientAppEventModal.open();
  }

  updateApplicationEventDetail() {
    if (this.mode === 'add') {
      this.addClientAppEventForm.submit();
    }
    else {
      this.editClientAppEventForm.submit();
    }
    this.addEditClientAppEventModal.close();
  }

  cancelApplicationEventChanges() {
    this.addEditClientAppEventModal.close();
  }

  removeApplicationEvent(_appEvent) {
    let confirmDelete = confirm("Are you sure you want to delete this application event?");
    if (confirmDelete) {
      this._appManagementControllerService.deleteApplicationEvent(_appEvent);
    }
  }



  assignHtml(_html) {
    //('in the card =>', this.messageHtml);
    const innerDiv = this.renderer.createElement('div');
    innerDiv.innerHTML = _html;
    //const text = this.renderer.createText('<input type="button" value"test">');
    //this.renderer.appendChild(innerDiv, text);

    if (this.infoDiv) {
      this.renderer.appendChild(this.infoDiv.nativeElement, innerDiv);
    }

  }
}
