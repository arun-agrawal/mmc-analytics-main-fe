import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MercerOSModule } from 'merceros-ui-components';

import { ClientAppEventRoutingModule } from './client-app-event-routing.module';
import { ClientAppEventComponent } from './client-app-event.component';
import { AddClientAppEventComponent } from './add-clientapp-event/add-clientapp-event.component';
import { EditClientAppEventComponent } from './edit-clientapp-event/edit-clientapp-event.component';

@NgModule({
  declarations: [
    ClientAppEventComponent,
    AddClientAppEventComponent,
    EditClientAppEventComponent,
  
  ],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    MercerOSModule,
    ClientAppEventRoutingModule
  ],
  providers: []

})
export class ClientAppEventModule { }
