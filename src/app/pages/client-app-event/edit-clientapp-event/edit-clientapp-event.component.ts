import {
  Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
  Input, Output, ViewChild, EventEmitter, ElementRef, ChangeDetectorRef, OnChanges
} from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { AppManagementControllerService } from '../../../domain/app-management/app-management-controller.service';
import { Application, ApplicationEvent } from '../../../common/interfaces';

@Component({
  selector: 'mmcanalytics-edit-clientapp-event',
  templateUrl: './edit-clientapp-event.component.html',
  styleUrls: ['./edit-clientapp-event.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default
})
export class EditClientAppEventComponent implements OnInit, OnChanges {
  @Output() onClose = new EventEmitter();
  @Output() onValidationError = new EventEmitter();

  @Input('appEvent') selectedAppEvent: ApplicationEvent;
  @Input('application') application: Application;

  @ViewChild('inputForm') inputForm: ElementRef;
  @ViewChild('eventElementWindow') eventElementWindow: any;

  public editApplicationEventForm: FormGroup;
  public clientAppUrl: any = '';
  public capturedEvent: any = { pageurl: '', type: '', element: {} }



  constructor(private _formBuilder: FormBuilder,
    private _changeDetectorRef: ChangeDetectorRef,
    private sanitizer: DomSanitizer,
    private _appManagementControllerService: AppManagementControllerService) {
    this.editApplicationEventForm = this._formBuilder.group({
      appEventPageURL: new FormControl(null),
      appEventName: new FormControl(null),
      appEventType: new FormControl(null),
      appEventCategory: new FormControl(null),
      appEventElement: new FormControl(null),
    });

  }

  ngOnInit(): void {
  }

  ngOnChanges(changes) {
    console.log('selected app event =>', this.selectedAppEvent);
    this.editApplicationEventForm.patchValue({
      appEventPageURL: this.selectedAppEvent.pageurl,
      appEventName: this.selectedAppEvent.name,
      appEventType: this.selectedAppEvent.type,
      appEventCategory: this.selectedAppEvent.category,
      appEventElement: JSON.stringify(this.selectedAppEvent.element),
    });

    this._changeDetectorRef.detectChanges();
  }

  submit(skipWarning: boolean = false) {
    this.selectedAppEvent.appid = this.trimValue(this.application._id),
      this.selectedAppEvent.pageurl = this.trimValue(this.editApplicationEventForm.value?.appEventPageURL),
      this.selectedAppEvent.name = this.trimValue(this.editApplicationEventForm.value?.appEventName),
      this.selectedAppEvent.type = this.trimValue(this.editApplicationEventForm.value?.appEventType),
      this.selectedAppEvent.category = this.trimValue(this.editApplicationEventForm.value?.appEventCategory),
      //this.selectedAppEvent.element = this.editApplicationEventForm.value?.appEventElement,
      this.selectedAppEvent.element = (this.editApplicationEventForm.value?.appEventElement !== '') ? JSON.parse(this.editApplicationEventForm.value?.appEventElement) : {},
      this._appManagementControllerService.updateApplicationEvent(this.selectedAppEvent);
    this.editApplicationEventForm.reset();
    this.onClose.emit();
  }

  trimValue(value) {
    return (value) ? value.trim() : value;
  }

  trimURL(url) {
    if (url && url.length > 0 && url.toString().lastIndexOf("/") === (url.length - 1)) {
      url = url.toString().substr(0, url.length - 1);
    }

    return url;
  }

  captureEventElement() {
    this.clientAppUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.application.url);
    this.eventElementWindow.open();

    this.capturedEvent.pageurl = '';
    this.capturedEvent.type = '';
    this.capturedEvent.element = {};

    this._changeDetectorRef.detectChanges();

    setTimeout(() => {
      // get reference to window inside the iframe
      //var wn = document.getElementById('clientAppIframe')..contentWindow;
      let win: any = window.frames;
      // postMessage arguments: data to send, target origin
      win.clientAppIframe?.postMessage({ capturetodefine: true }, this.application.url);
      window.addEventListener("message", this.showCapturedEvent.bind(this), false);
    }, 3000);
  }



  showCapturedEvent(event) {
    console.log('In Parent =>', event.origin, event.data);
    if (!event || (this.trimURL(event.origin) != this.trimURL(this.application.url))) {
      //if (event.origin != this.application.url) {
      // something from an unknown domain, let's ignore it
      return;
    }
    this.capturedEvent.pageurl = event.data.pageurl;
    this.capturedEvent.type = event.data.event?.type;
    this.capturedEvent.element = event.data.event?.element;
    this._changeDetectorRef.detectChanges();
  }

  setCaptureEventElement() {
    this.editApplicationEventForm.patchValue({
      appEventPageURL: this.capturedEvent.pageurl,
      appEventType: this.capturedEvent.type,
      appEventElement: JSON.stringify(this.capturedEvent.element)
    });
    this.eventElementWindow.close();
  }

}
