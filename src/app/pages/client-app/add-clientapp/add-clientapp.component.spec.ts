import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddClientAppComponent } from './add-clientapp.component';

describe('AddClientAppComponent', () => {
  let component: AddClientAppComponent;
  let fixture: ComponentFixture<AddClientAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddClientAppComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddClientAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
