import {
  Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
  Input, Output, ViewChild, EventEmitter, ElementRef, ChangeDetectorRef
} from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AppManagementControllerService } from '../../../domain/app-management/app-management-controller.service';

@Component({
  selector: 'mmcanalytics-add-clientapp',
  templateUrl: './add-clientapp.component.html',
  styleUrls: ['./add-clientapp.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default
})
export class AddClientAppComponent implements OnInit {
  @Output() onClose = new EventEmitter();
  @Output() onValidationError = new EventEmitter();

  @ViewChild('inputForm') inputForm: ElementRef;

  public addApplicationForm: FormGroup;
  selectedOwners: any = [];

  constructor(private _formBuilder: FormBuilder,
    private _changeDetectorRef: ChangeDetectorRef,
    private _appManagementControllerService: AppManagementControllerService) {
    this.addApplicationForm = this._formBuilder.group({
      applicationName: new FormControl(null),
      applicationDesc: new FormControl(null),
      applicationURL: new FormControl(null),
      applicationOwners: new FormControl(null),
    });
  }

  ngOnInit(): void {
  }

  submit(skipWarning: boolean = false) {
    let app: any = {
      name: this.trimValue(this.addApplicationForm.value?.applicationName),
      description: this.trimValue(this.addApplicationForm.value?.applicationDesc),
      url: this.trimValue(this.addApplicationForm.value?.applicationURL),
      owners: this.selectedOwners,
    };

    this._appManagementControllerService.registerApplication(app);
    this.addApplicationForm.reset();
    this.onClose.emit();
  }

  trimValue(value) {
    return (value) ? value.trim() : value;
  }

  public openColleagueDirectory(_win) {
    _win.open();
  }

  public updateOwner(_owners, _popupwin) {
    console.log('updateOwner =>', _owners);

    if (_owners !== null && _owners.length > 0) {
      for (var i = 0; i < _owners.length; i++) {
        this.selectedOwners.push({
          fname: _owners[i].fname,
          lname: _owners[i].lname,
          email: _owners[i].email
        });
      }
    }
    this._changeDetectorRef.detectChanges();
    _popupwin.close();
  }

  public removeOwner(_owner) {
    console.log('remove owner =>', _owner);

    let newSelection = [];
    for (var i = 0; i < this.selectedOwners.length; i++) {
      if (this.selectedOwners[i].email.toLowerCase() != _owner.email.trim()) {
        newSelection.push(this.selectedOwners[i]);
      }
    }
    this.selectedOwners = newSelection;
    this._changeDetectorRef.detectChanges();
  }

}
