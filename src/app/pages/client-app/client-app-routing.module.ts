import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientAppComponent } from './client-app.component';


const routes: Routes = [{ path: '', component: ClientAppComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientAppRoutingModule { }
