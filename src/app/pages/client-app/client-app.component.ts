import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { MosModalComponent, PaginationInfo } from 'merceros-ui-components';
import { AddClientAppComponent } from './add-clientapp/add-clientapp.component';
import { EditClientAppComponent } from './edit-clientapp/edit-clientapp.component';

import { AppManagementControllerService } from '../../domain/app-management/app-management-controller.service';
import {environment } from '../../../environments/environment';
import { cloneDeep } from 'lodash-es';

export class AppInfo { title:string = ""; type:string = ""; headertext:string = ""; content:string[] = []; footertext:string = "" }

@Component({
  templateUrl: './client-app.component.html',
  styleUrls: ['./client-app.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClientAppComponent implements OnInit {

  public applications: any = [];
  public mode: string = 'add';
  public selectedApp: any;
  public appInfo: AppInfo;

  @ViewChild('addEditClientAppModal') addEditClientAppModal: MosModalComponent;
  @ViewChild('readOnlyInfoClientAppModal') readOnlyInfoClientAppModal: MosModalComponent;
  @ViewChild('infoDiv', { static: false }) infoDiv: ElementRef;

  @ViewChild('addClientAppForm') addClientAppForm: AddClientAppComponent;
  @ViewChild('editClientAppForm') editClientAppForm: EditClientAppComponent;

  constructor(private renderer: Renderer2,
    private _router: Router,
    private _appManagementControllerService: AppManagementControllerService) {
    this._appManagementControllerService.loadAllApplications();
    this._appManagementControllerService._allApps$.subscribe((apps: any) => {
      this.applications = (apps && apps.success) ? apps.data : [];
    });
  }

  ngOnInit(): void {

  }


  openModal(_mode, _app) {
    this.mode = _mode;
    this.selectedApp = (_app) ? cloneDeep(_app) : null;
    this.addEditClientAppModal.open();
  }

  updateApplicationDetail() {
    if (this.mode === 'add') {
      this.addClientAppForm.submit();
    }
    else {
      this.editClientAppForm.submit();
    }
    this.addEditClientAppModal.close();

  }

  cancelApplicationChanges() {
    this.addEditClientAppModal.close();
  }

  removeApplication(_app) {
    let confirmDelete = confirm("Are you sure to delete this application from capturing events?");
    if (confirmDelete) {
      this._appManagementControllerService.deleteApplication(_app);
    }
  }

  showApplicationIntegrationScript(_app) {
    this.appInfo = new AppInfo();
    this.appInfo.title = 'Application integration script';
    this.appInfo.type = 'integration-script';
    this.appInfo.headertext = 'You need to add below script in your application page to allow event capture -';
    this.appInfo.content.push(`<script src="${environment.SCRIPT_PATH}"></script>`);
    this.appInfo.content.push(`<script>`);
    this.appInfo.content.push(`//client script`); 
    this.appInfo.content.push(`MMCAnalytics.appid = '${_app._id}';`);
    this.appInfo.content.push(`MMCAnalytics.env = current_environment;`);
    this.appInfo.content.push(`MMCAnalytics.userid = current_userid;`);  
    this.appInfo.content.push(`</script>`);
    this.appInfo.footertext = "Replace current_environment and current_userid with appropriate value or variables in your code"; 

    //this.assignHtml(this.appInfo.content);
    this.readOnlyInfoClientAppModal?.open();
  }

  openApplicationEvents(_app) {
    localStorage.setItem('currentClientApp',_app._id);
    this._router.navigate(['/clientappevent']);
  }

  assignHtml(_html) {
    //('in the card =>', this.messageHtml);
    const innerDiv = this.renderer.createElement('div');
    innerDiv.innerHTML = _html;
    //const text = this.renderer.createText('<input type="button" value"test">');
    //this.renderer.appendChild(innerDiv, text);

    if (this.infoDiv) {
      this.renderer.appendChild(this.infoDiv.nativeElement, innerDiv);
    }

  }
}
