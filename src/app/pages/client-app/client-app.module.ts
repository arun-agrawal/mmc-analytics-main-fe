import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MercerOSModule } from 'merceros-ui-components';

import { ClientAppRoutingModule } from './client-app-routing.module';
import { ClientAppComponent } from './client-app.component';
import { AddClientAppComponent } from './add-clientapp/add-clientapp.component';
import { EditClientAppComponent } from './edit-clientapp/edit-clientapp.component';
import { ColleagueDirectoryComponent } from './colleague-directory/colleague-directory.component';
import { DirectoryService } from '../../shared/services';

@NgModule({
  declarations: [
    ClientAppComponent,
    AddClientAppComponent,
    EditClientAppComponent,
    ColleagueDirectoryComponent
  ],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    MercerOSModule,
    ClientAppRoutingModule
  ],
  providers: [DirectoryService]

})
export class ClientAppModule { }
