import {
  Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
  ChangeDetectorRef, ElementRef, ViewChild, Input, Output,
  EventEmitter
} from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../../../router.animations';

import { DirectoryService } from '../../../shared/services';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'mercer-colleague-directory',
  templateUrl: './colleague-directory.component.html',
  styleUrls: ['./colleague-directory.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default,
  animations: [routerTransition()]
})
export class ColleagueDirectoryComponent implements OnInit {

  selectedColleagues: any[] = [];

  public firstname: String = '';
  public lastname: String = '';
  public fullname: String = '';

  public searchResults: any[] = [];
  public searchClicked: boolean = false;
  public loading: boolean = false;

  @Output() onsave: EventEmitter<any> = new EventEmitter<any>();
  constructor(private _changeDetectorRef: ChangeDetectorRef, private _DirectoryService: DirectoryService) {

  }

  ngOnInit() {
    this.searchClicked = false;
    this.searchResults = [];
    this.loading = false;
  }

  searchColleague(event) {

    console.log('searchColleagues =>', event.keyCode, event.type, event)

    if ((event.type === 'click') || (event.type === 'keyup' && event.keyCode === 13)) {
      var parser = new DOMParser();
      this.searchResults = [];
      this.searchClicked = false;

      if (this.firstname.toString().trim() === '' && this.lastname.toString().trim() === '') {
        return;
      }

      this.loading = true;
      this._DirectoryService.GetMatchingColleagues(this.firstname.toString(), this.lastname.toString())
        .subscribe(res => {
          console.log('result from service =>', res)

          var xmlData = parser.parseFromString(res.result, "application/xml");

          console.log('service output =>', xmlData, xmlData.childNodes.length);
          var persons = xmlData.getElementsByTagName('Result');

          if (persons !== null && persons.length > 0) {
            for (var i = 0; i < persons.length; i++) {
              this.searchResults.push({
                fname: persons[i].getAttribute('prfFirstName'),
                lname: persons[i].getAttribute('prfLastName'),
                email: persons[i].getAttribute('email')
              });
            }
          }
          this.searchClicked = true;
          this.loading = false;
          this._changeDetectorRef.detectChanges();
        });

    }
  }

  searchColleagueContains(event) {

    console.log('searchColleagues =>', event.keyCode, event.type, event)

    if ((event.type === 'click') || (event.type === 'keyup' && event.keyCode === 13)) {
      this.searchResults = [];
      this.searchClicked = false;

      if (this.fullname.toString().trim() === '') {
        return;
      }

      let apiCalls = [];
      this.loading = true;
      apiCalls.push(this._DirectoryService.GetMatchingColleaguesFirstNameContains(this.fullname.toString()));
      apiCalls.push(this._DirectoryService.GetMatchingColleaguesLastNameContains(this.fullname.toString()));

      forkJoin(apiCalls)
        .subscribe((values: any) => {
          console.log('result from service =>', values)
          this.appendSearchResult(values[0].result);
          this.appendSearchResult(values[1].result);
          this.searchResults.sort((a, b) => (a.fname > b.fname) ? 1 : -1);
          this.searchClicked = true;
          this.loading = false;
          this._changeDetectorRef.detectChanges();
        });

    }
  }

  private appendSearchResult(results: any) {
    var parser = new DOMParser();
    var xmlData = parser.parseFromString(results, "application/xml");

    console.log('service output =>', xmlData, xmlData.childNodes.length);
    var persons = xmlData.getElementsByTagName('Result');

    if (persons !== null && persons.length > 0) {
      for (var i = 0; i < persons.length; i++) {
        this.searchResults.push({
          fname: persons[i].getAttribute('prfFirstName'),
          lname: persons[i].getAttribute('prfLastName'),
          email: persons[i].getAttribute('email')
        });
      }
    }

  }

  public onChecked(event) {
    var isChecked = event.target.checked;
    var selectedValue = event.target.value;

    var selfirstname = selectedValue.split('|')[0];
    var sellastname = selectedValue.split('|')[1];
    var selemail = selectedValue.split('|')[2];

    if (isChecked) {
      this.selectedColleagues.push({ fname: selfirstname, lname: sellastname, email: selemail })
    }
    else {
      var tempSelection = [];
      for (var i = 0; i < this.selectedColleagues.length; i++) {
        if (this.selectedColleagues[i].email.toString().toLowerCase() !== selemail.toString().trim()) {
          tempSelection.push(this.selectedColleagues[i]);
        }
      }
      this.selectedColleagues = tempSelection;
    }
    console.log('selcted colleagues =>', this.selectedColleagues);
  }

  public removeColleague(_colleague) {
    console.log('remove colleague =>', _colleague);

    let newSelection = [];
    for (var i = 0; i < this.selectedColleagues.length; i++) {
      if (this.selectedColleagues[i].email.toLowerCase() != _colleague.email.trim()) {
        newSelection.push(this.selectedColleagues[i]);
      }
    }
    this.selectedColleagues = newSelection;
  }

  public save() {
    this.onsave.emit(this.selectedColleagues);
    this.reset();

  }

  cancel() {
    this.onsave.emit([]);
    this.reset();
  }

  reset() {
    this.selectedColleagues = [];
    this.firstname = '';
    this.lastname = '';
    this.searchResults = [];
    this.searchClicked = false;
  }


}
