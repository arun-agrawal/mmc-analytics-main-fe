import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditClientAppComponent } from './edit-clientapp.component';

describe('EditClientAppComponent', () => {
  let component: EditClientAppComponent;
  let fixture: ComponentFixture<EditClientAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditClientAppComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditClientAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
