import {
  Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
  Input, Output, ViewChild, EventEmitter, ElementRef, ChangeDetectorRef, OnChanges
} from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AppManagementControllerService } from '../../../domain/app-management/app-management-controller.service';
import { Application } from '../../../common/interfaces';

@Component({
  selector: 'mmcanalytics-edit-clientapp',
  templateUrl: './edit-clientapp.component.html',
  styleUrls: ['./edit-clientapp.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default
})
export class EditClientAppComponent implements OnInit, OnChanges {
  @Output() onClose = new EventEmitter();
  @Output() onValidationError = new EventEmitter();

  @Input('application') selectedApp: Application;

  @ViewChild('inputForm') inputForm: ElementRef;

  public editApplicationForm: FormGroup;
  selectedOwners: any = [];

  constructor(private _formBuilder: FormBuilder,
    private _changeDetectorRef: ChangeDetectorRef,
    private _appManagementControllerService: AppManagementControllerService) {
    this.editApplicationForm = this._formBuilder.group({
      applicationName: new FormControl(null),
      applicationDesc: new FormControl(null),
      applicationURL: new FormControl(null),
      applicationOwners: new FormControl(null),
    });
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes) {
    console.log('selected app =>', this.selectedApp);
    this.editApplicationForm.patchValue({
      applicationName: this.selectedApp.name,
      applicationDesc: this.selectedApp.description,
      applicationURL: this.selectedApp.url
    });
    this.selectedOwners = this.selectedApp.owners;
    this._changeDetectorRef.detectChanges();
  }

  submit(skipWarning: boolean = false) {
    this.selectedApp.name = this.trimValue(this.editApplicationForm.value?.applicationName);
    this.selectedApp.description = this.trimValue(this.editApplicationForm.value?.applicationDesc);
    this.selectedApp.url = this.trimValue(this.editApplicationForm.value?.applicationURL);
    this.selectedApp.owners = this.selectedOwners;

    this._appManagementControllerService.updateApplication(this.selectedApp);
    this.editApplicationForm.reset();
    this.onClose.emit();
  }

  trimValue(value) {
    return (value) ? value.trim() : value;
  }

  public openColleagueDirectory(_win) {
    _win.open();
  }

  public updateOwner(_owners, _popupwin) {
    console.log('updateOwner =>', _owners);

    if (_owners !== null && _owners.length > 0) {
      for (var i = 0; i < _owners.length; i++) {
        this.selectedOwners.push({
          fname: _owners[i].fname,
          lname: _owners[i].lname,
          email: _owners[i].email
        });
      }
    }
    this._changeDetectorRef.detectChanges();
    _popupwin.close();
  }

  public removeOwner(_owner) {
    console.log('remove owner =>', _owner);

    let newSelection = [];
    for (var i = 0; i < this.selectedOwners.length; i++) {
      if (this.selectedOwners[i].email.toLowerCase() != _owner.email.trim()) {
        newSelection.push(this.selectedOwners[i]);
      }
    }
    this.selectedOwners = newSelection;
    this._changeDetectorRef.detectChanges();
  }

}
