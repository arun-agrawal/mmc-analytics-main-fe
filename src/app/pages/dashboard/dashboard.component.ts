import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';

import { AppManagementControllerService } from '../../domain/app-management/app-management-controller.service';
import { environment } from '../../../environments/environment';
import { Application, ApplicationEvent } from '../../common/interfaces';


@Component({
  selector: 'mercer-app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {

  public applications: Application[] = [];
  public selectedAppId: string = '';
  public appEvents: ApplicationEvent[] = [];

  public lineChartLabels = [];
  public lineChartData = [];

  public pieChartLabels = [];
  public pieChartData = [];

  public barChartLabels = [];
  public barChartData = [];

  refreshHandle: any;

  constructor(private _router: Router,
    private _changeDetectorRef: ChangeDetectorRef,
    private _appManagementControllerService: AppManagementControllerService) {
    this._appManagementControllerService.loadAllApplications();
    this._appManagementControllerService._allApps$.subscribe((apps: any) => {
      this.applications = (apps && apps.success) ? apps.data : [];
      setTimeout(() => { this._changeDetectorRef.detectChanges() }, 100);
    });
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.selectedAppId = localStorage.getItem('currentClientApp');
      this._changeDetectorRef.detectChanges();
      this.onApplicationSelect(null);
    }, 100);

    //refresh the dataset in every 5 sec.
    /*
    this.refreshHandle = setInterval(() => {
      if (this.selectedAppId && this.selectedAppId !== '') {
        this._appManagementControllerService.loadEventLogsGroupBy_EventType_YYYYMM(this.selectedAppId);
      }
    }, 5000);
    */
  }

  clientAppRegistration() {
    this._router.navigate(['clientapp']);
  }

  onApplicationSelect(event) {
    console.log("on app select =>", event?.target?.value, this.selectedAppId)
    if (event) {
      this.selectedAppId = event?.target?.value;
      localStorage.setItem('currentClientApp', this.selectedAppId);
    }

    this._appManagementControllerService.loadApplicationEvents(this.selectedAppId);
    this._appManagementControllerService._currentAppEvents$.subscribe(
      (resultAppEvents: any) => {
        console.log('resultAppEvents =>', resultAppEvents);
        this.appEvents = (resultAppEvents && resultAppEvents.success) ? resultAppEvents.data : [];

        this._appManagementControllerService.loadEventLogsGroupBy_EventType_YYYYMM(this.selectedAppId);
        this._appManagementControllerService._eventLogsGroupByEventTypeOnYYYYMM$.subscribe(
          (logs: any) => {
            console.log('logs =>', logs);
            let eventlogs = {};
            if (logs && logs.success && logs.data) {

              for (let i = 0; i < logs.data.length; i++) {
                let key = logs.data[i].eventId;
                if (!eventlogs[key]) {
                  eventlogs[key] = { event: {}, data: [] };
                }
                let eventType = this.appEvents.find(x => x._id === logs.data[i].eventId);
                eventlogs[key].event = eventType;
                eventlogs[key].data.push({
                  "year": logs.data[i].year,
                  "month": logs.data[i].month,
                  "count": logs.data[i].eventCount
                });
              }

              console.log('event logs => ', eventlogs);


            }

            //now draw the charts
            this.drawLineChart(eventlogs);
            this.drawPieChart(eventlogs);
            this.drawBarChart(eventlogs);

          });
      }
    );

    /*
    this._appManagementControllerService.loadApplicationEvents(this.selectedApp?._id);
    this._appManagementControllerService._currentAppEvents$.subscribe((appEvents: any) => {
      this.applicationEvents = (appEvents && appEvents.success) ? appEvents.data : [];
      console.log("app events =>", appEvents, this.selectedApp)
      this._changeDetectorRef.detectChanges();
    });
    */
  }


  drawLineChart(_eventlogs) {
    //console.log('_eventlogs =>', _eventlogs);
    /*
     this.lineChartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
    
     this.lineChartData = [
       { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
       { data: [50, 39, 40, 71, 46, 75, 60], label: 'Series B' },
     ];
     */

    let timeRange = this.getLineChartTimeRange();
    this.lineChartLabels = timeRange.map(r => r.month_name);
    this.lineChartData = [];

    if (this.selectedAppId && _eventlogs && Object.keys(_eventlogs).length > 0) {

      for (let i = 0; i < Object.keys(_eventlogs).length; i++) {
        let eventMonthlyData = [];
        let eventTypeId = Object.keys(_eventlogs)[i];
        for (let j = 0; j < timeRange.length; j++) {
          let monthData = _eventlogs[eventTypeId].data?.find(d => d.year === timeRange[j].year.toString() && d.month === timeRange[j].month.toString());
          //console.log('matched monthdata =>', timeRange[j],  _eventlogs[eventTypeId].data, monthData)
          if (monthData) {
            eventMonthlyData.push(monthData?.count);
          }
          else {
            eventMonthlyData.push(0);
          }
        }

        this.lineChartData.push({
          data: eventMonthlyData,
          label: _eventlogs[eventTypeId]?.event?.name,
        })
      }

      console.log('Calculated line chart data =>', this.lineChartLabels, this.lineChartData)


    }

    this._changeDetectorRef.detectChanges();
  }

  getLineChartTimeRange() {
    let timeRange = [];
    let month_names = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let dt = new Date();
    let curr_year = dt.getFullYear();
    let curr_mm = dt.getMonth() + 1;

    console.log('Current Year and month =>', curr_year, curr_mm);
    if (curr_mm >= 6) {
      for (let m = 5; m >= 0; m--) {
        let mon = curr_mm - m;
        timeRange.push({
          "year": curr_year,
          "month_name": month_names[mon],
          "month": mon
        })
      }
    }
    else {
      //first capture last year's month
      let mon = 6 + curr_mm;
      while (mon <= 12) {
        mon++;
        timeRange.push({ "year": curr_year - 1, "month_name": month_names[mon], "month": mon });
      }

      //now, add this year's months
      mon = 0;
      while (mon < curr_mm) {
        mon++;
        timeRange.push({ "year": curr_year, "month_name": month_names[mon], "month": mon });
      }
    }

    console.log('timeRange =>', timeRange);
    return timeRange;
  }


  drawBarChart(_eventlogs) {
    this.barChartLabels = ['2014', '2015', '2016', '2017', '2018', '2019', '2020'];
    this.barChartData = [
      { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
      { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
    ];
    this._changeDetectorRef.detectChanges();
  }

  drawPieChart(_eventlogs) {
    /*
     this.pieChartLabels = [['Download', 'Sales'], ['In', 'Store', 'Sales'], 'Mail Sales'];
    this.pieChartData = [300, 500, 100]; 
     */

    this.pieChartLabels = [];
    this.pieChartData = [];

    if (this.selectedAppId && _eventlogs && Object.keys(_eventlogs).length > 0) {
      let timeRange = this.getLineChartTimeRange();

      for (let i = 0; i < Object.keys(_eventlogs).length; i++) {
        let eventMonthlyData = [];
        let eventTypeId = Object.keys(_eventlogs)[i];

        this.pieChartLabels.push(_eventlogs[eventTypeId].event.name);
        let totalHit = 0;
        for (let j = 0; j < _eventlogs[eventTypeId].data?.length; j++) {
          totalHit = totalHit + _eventlogs[eventTypeId].data[j].count;
        }
        this.pieChartData.push(totalHit);
      }

      this._changeDetectorRef.detectChanges();
    }
  }


}
