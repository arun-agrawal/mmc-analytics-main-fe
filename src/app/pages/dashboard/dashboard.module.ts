import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MercerOSModule } from 'merceros-ui-components';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ComponentsModule } from '../../common/components/components.module';
import { DomainModule } from '../../domain/domain.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

import { MMCAnalyticsChartsModule } from './mmc-analytics-charts/mmc-analytics-charts.module';




@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    MercerOSModule,
    FormsModule, ReactiveFormsModule,
    DashboardRoutingModule,
    DomainModule,
    MMCAnalyticsChartsModule,
    
  ],
})
export class DashboardModule { }
