import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarChartMMCAnalyticsComponent } from './bar-chart.component';

describe('BarChartMMCAnalyticsComponent', () => {
  let component: BarChartMMCAnalyticsComponent;
  let fixture: ComponentFixture<BarChartMMCAnalyticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarChartMMCAnalyticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarChartMMCAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
