import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input, OnChanges } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

@Component({
  selector: 'mmc-analytics-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default
})
export class LineChartMMCAnalyticsComponent implements OnInit, OnChanges {

  @Input('Y_ChartData') lineChartData: ChartDataSets[];
  @Input('X_ChartLabels') lineChartLabels: Label[];
  
  /*
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [50, 39, 40, 71, 46, 75, 60], label: 'Series B' },
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  */

  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    annotation: {}
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(_changes) {

  }

}
