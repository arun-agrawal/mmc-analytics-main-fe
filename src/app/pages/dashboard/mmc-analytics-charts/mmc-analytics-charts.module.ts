import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';
import { LineChartMMCAnalyticsComponent } from './line-chart/line-chart.component';
import { BarChartMMCAnalyticsComponent } from './bar-chart/bar-chart.component';
import { PieChartMMCAnalyticsComponent } from './pie-chart/pie-chart.component';
import { RadarChartMMCAnalyticsComponent } from './radar-chart/radar-chart.component';
import { ScatterChartMMCAnalyticsComponent } from './scatter-chart/scatter-chart.component';

const chartComponents = [
  LineChartMMCAnalyticsComponent,
  BarChartMMCAnalyticsComponent,
  PieChartMMCAnalyticsComponent,
  RadarChartMMCAnalyticsComponent,
  ScatterChartMMCAnalyticsComponent
]

@NgModule({
  declarations: [chartComponents],
  imports: [
    CommonModule,
    ChartsModule
  ],
  exports: [chartComponents]
})
export class MMCAnalyticsChartsModule { }
