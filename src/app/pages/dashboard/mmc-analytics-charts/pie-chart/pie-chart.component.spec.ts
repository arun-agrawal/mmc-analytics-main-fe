import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PieChartMMCAnalyticsComponent } from './pie-chart.component';

describe('PieChartMMCAnalyticsComponent', () => {
  let component: PieChartMMCAnalyticsComponent;
  let fixture: ComponentFixture<PieChartMMCAnalyticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PieChartMMCAnalyticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PieChartMMCAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
