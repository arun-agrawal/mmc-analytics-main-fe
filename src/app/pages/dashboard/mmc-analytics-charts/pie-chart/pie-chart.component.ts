import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input, OnChanges } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

@Component({
  selector: 'mmc-analytics-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default
})
export class PieChartMMCAnalyticsComponent implements OnInit, OnChanges {
  @Input('ChartLabels') pieChartLabels: Label[] = [];
  @Input('ChartData') pieChartData: SingleDataSet = [];

  
  //public pieChartLabels: Label[] = [['Download', 'Sales'], ['In', 'Store', 'Sales'], 'Mail Sales'];
  //public pieChartData: SingleDataSet = [300, 500, 100];


  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };

  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  constructor() {

  }

  ngOnInit() {

  }

  ngOnChanges(_changes) {
    //monkeyPatchChartJsTooltip();
    //monkeyPatchChartJsLegend();
  }

}
