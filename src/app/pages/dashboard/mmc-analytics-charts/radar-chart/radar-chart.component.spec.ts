import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadarChartMMCAnalyticsComponent } from './radar-chart.component';

describe('RadarChartMMCAnalyticsComponent', () => {
  let component: RadarChartMMCAnalyticsComponent;
  let fixture: ComponentFixture<RadarChartMMCAnalyticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadarChartMMCAnalyticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadarChartMMCAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
