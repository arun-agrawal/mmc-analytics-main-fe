import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScatterChartMMCAnalyticsComponent } from './scatter-chart.component';

describe('ScatterChartMMCAnalyticsComponent', () => {
  let component: ScatterChartMMCAnalyticsComponent;
  let fixture: ComponentFixture<ScatterChartMMCAnalyticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScatterChartMMCAnalyticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScatterChartMMCAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
