import { Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { ToastService } from '../../features/shared/services/toast.service';
import { FakeauthService } from '../../shared/fake-auth';

 
@Component({
  selector: 'mercer-app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent {
  constructor(private _router: Router, private _toastService: ToastService  ,private _fakeauthService: FakeauthService) {}

  onLogin(user) {
    console.log('login attempt =>', user);
    let isValid = this._fakeauthService.ValidateUser(user.username);
    if (isValid) {
      this._router.navigate(['dashboard']);
    }
    else {
      //show invalid user toast message
      this._toastService.createAlert({message: "You are not an authorised user for this application", autoDismiss: true});
    }
  }
}
