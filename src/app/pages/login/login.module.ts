import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MercerOSModule } from 'merceros-ui-components';
import { AuthenticationModule } from 'ngpd-merceros-authentication-fe-components';

import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';

const COMPONENTS = [ LoginComponent ];

@NgModule({
  imports: [
    CommonModule,
    MercerOSModule,
    AuthenticationModule,
    LoginRoutingModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class LoginModule { }
