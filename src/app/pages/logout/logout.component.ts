import { Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthFacadeService } from 'ngpd-merceros-authentication-fe-components';
import { take } from 'rxjs/operators';

@Component({
  selector: 'mercer-app-auth-logout',
  templateUrl: './logout.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogoutComponent {
  constructor(
    private _authFacade: AuthFacadeService,
    private _route: ActivatedRoute,
  ) {
    this._route.queryParams.pipe(take(1)).subscribe(params => {
      if (params && params.slo) {
        this._authFacade.singleLogoutSuccessMessage();
      }
    });
  }
}
