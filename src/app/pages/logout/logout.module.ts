import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MercerOSModule } from 'merceros-ui-components';
import { AuthenticationModule } from 'ngpd-merceros-authentication-fe-components';

import { LogoutComponent } from './logout.component';
import { LogoutRoutingModule } from './logout-routing.module';

const COMPONENTS = [ LogoutComponent ];

@NgModule({
  imports: [
    CommonModule,
    MercerOSModule,
    AuthenticationModule,
    LogoutRoutingModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class LogoutModule { }
