import { ChangeDetectionStrategy, Component, ViewEncapsulation, ChangeDetectorRef, OnInit, AfterContentChecked } from '@angular/core';
import { Router } from '@angular/router';
import { MosConfigurationService } from 'merceros-ui-components';
import { Observable, of, from } from 'rxjs';
import { AuthFacadeService } from 'ngpd-merceros-authentication-fe-components';

import { englishLabels } from '../../language-pack/english';
import { deutchLabels } from '../../language-pack/deutch';

import { FakeauthService } from '../../shared/fake-auth';

@Component({
  selector: 'mercer-app-page-container',
  templateUrl: './page-wrapper.component.html',
  styleUrls: ['./page-wrapper.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default,
})
export class PageWrapperComponent implements OnInit, AfterContentChecked {

  loggedIn$: Observable<boolean>;
  loggedIn: boolean = false;
  user: any;

  constructor(public mosConfig: MosConfigurationService, private _authFacade: AuthFacadeService,
    private _router: Router,
    private _fakeauthService: FakeauthService, private _changeDetectorRef: ChangeDetectorRef) {
    //this.loggedIn$ = this._authFacade.loggedIn$;
    
    //this.loggedIn$ = from(this._fakeauthService.IsLoggedIn());
    this.user = {name: ''};
  }

  ngOnInit() {
    this._changeDetectorRef.detectChanges();
  }

  ngAfterContentChecked() {
    this._fakeauthService.IsLoggedIn().then(isAuthenticated => {
      this.loggedIn = isAuthenticated;
      this.user = this._fakeauthService.GetCurrentUser();
      this._changeDetectorRef.detectChanges();
    });
   // console.log('************* ngAfterContentChecked ******************', this.loggedIn)
    //console.log('user logged in =>', this._fakeauthService.IsLoggedIn());
    
  }

  changeLanguage(language: string) {
    if (language === 'english') {
      this._authFacade.updateLabels(englishLabels);
      localStorage.setItem('language', 'en-US');
    } else if (language === 'deutch') {
      this._authFacade.updateLabels(deutchLabels);
      localStorage.setItem('language', 'de-DE');
    } else {
      this._authFacade.revertLabelsToDefault();
    }
  }

  logout() {
    //this._authFacade.logout();
    this._fakeauthService.LogOut();
    this._fakeauthService.IsLoggedIn().then(isAuthenticated => {
      this.loggedIn = isAuthenticated;
      this._router.navigate(['login']);
      this._changeDetectorRef.detectChanges();
    });
  }

}
