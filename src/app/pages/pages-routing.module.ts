import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard, NonAuthGuard } from 'ngpd-merceros-authentication-fe-components';

import { PageWrapperComponent } from './page-wrapper/page-wrapper.component';

import { FakeAuthGuardService } from '../shared/fake-auth';

const pagesRoutes: Routes = [
  {
    path: '',
    component: PageWrapperComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
      },
      {
        path: 'login',
        loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
        canActivate: [NonAuthGuard],
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
        canActivate: [FakeAuthGuardService],
      },
      {
        path: 'clientapp',
        loadChildren: () => import('./client-app/client-app.module').then(m => m.ClientAppModule),
        canActivate: [FakeAuthGuardService],
      },
      {
        path: 'clientappevent',
        loadChildren: () => import('./client-app-event/client-app-event.module').then(m => m.ClientAppEventModule),
        canActivate: [FakeAuthGuardService],
      },
      
      {
        path: 'user-profile',
        loadChildren: () => import('./user-profile/user-profile.module').then(m => m.UserProfileModule),
        canActivate: [FakeAuthGuardService],
      },
      {
        path: 'logout',
        loadChildren: () => import('./logout/logout.module').then(m => m.LogoutModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(pagesRoutes)],
  exports: [RouterModule],
  declarations: [],
})
export class PagesRoutingModule {}
