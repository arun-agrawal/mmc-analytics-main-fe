import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MercerOSModule } from 'merceros-ui-components';

import { ComponentsModule } from '../common/components/components.module';
import { PageWrapperComponent } from './page-wrapper/page-wrapper.component';
import { PagesRoutingModule } from './pages-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    MercerOSModule,
    PagesRoutingModule,
  ],
  declarations: [PageWrapperComponent],
  exports: [PageWrapperComponent],
})
export class PageModule { }
