import { AfterViewInit, ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { AuthFacadeService } from 'ngpd-merceros-authentication-fe-components';

@Component({
  selector: 'mercer-app-auth-registration',
  template: '',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationComponent implements AfterViewInit {
  public dataWithoutLanguageAndUsername = {
    firstName: 'Franklin',
    lastName: 'Lee',
    country: 'Zimbabwe',
    dob: 'January 1, 1988',
    favoriteFood: 'Spaghetti',
  };

  public dataWithoutLanguage = {
    firstName: 'Franklin',
    lastName: 'Lee',
    country: 'Zimbabwe',
    dob: 'January 1, 1988',
    favoriteFood: 'Spaghetti',
    username: 'franklin+columbus28@halopowered.com'
  };

  public dataWithoutCountry = {
    firstName: 'Franklin',
    lastName: 'Lee',
    language: 'english',
    dob: 'January 1, 1988',
    favoriteFood: 'Spaghetti',
    username: 'franklin+columbus27@halopowered.com'
  };

  public dataWithoutUsername = {
    firstName: 'Franklin',
    lastName: 'Lee',
    country: 'Zimbabwe',
    language: 'spanish',
    dob: 'January 1, 1988',
    favoriteFood: 'Spaghetti',
  };

  public dataWithExistingUsername = {
    firstName: 'Franklin',
    lastName: 'Lee',
    country: 'Zimbabwe',
    language: 'english',
    dob: 'January 1, 1988',
    favoriteFood: 'Spaghetti',
    username: 'franklin+columbus27@halopowered.com'
  };


  public dataWithEverything = {
    firstName: 'Franklin',
    lastName: 'Lee',
    country: 'Zimbabwe',
    language: 'english',
    dob: 'January 1, 1988',
    favoriteFood: 'Spaghetti',
    username: 'franklin+columbus28@halopowered.com'
  };

  constructor(private _authFacade: AuthFacadeService) {}

  ngAfterViewInit() {
    // this._authFacade.enterPrefilledRegistration(this.dataWithoutLanguageAndUsername);
    // this._authFacade.enterPrefilledRegistration(this.dataWithoutLanguage);
    // this._authFacade.enterPrefilledRegistration(this.dataWithoutCountry);
    // this._authFacade.enterPrefilledRegistration(this.dataWithoutUsername);
    // this._authFacade.enterPrefilledRegistration(this.dataWithExistingUsername);
    this._authFacade.enterPrefilledRegistration(this.dataWithEverything);
  }
}
