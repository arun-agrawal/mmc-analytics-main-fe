import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MercerOSModule } from 'merceros-ui-components';
import { AuthenticationModule } from 'ngpd-merceros-authentication-fe-components';

import { RegistrationComponent } from './registration.component';
import { RegistrationRoutingModule } from './registration-routing.module';

const COMPONENTS = [
  RegistrationComponent,
];

@NgModule({
  imports: [
    CommonModule,
    MercerOSModule,
    AuthenticationModule,
    RegistrationRoutingModule,
  ],
  declarations: COMPONENTS,
})
export class RegistrationModule { }
