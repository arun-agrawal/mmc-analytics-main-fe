import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { AuthFacadeService } from 'ngpd-merceros-authentication-fe-components';

@Component({
  selector: 'mercer-app-auth-user-profile',
  templateUrl: './user-profile.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserProfileComponent {
  constructor(private _authFacade: AuthFacadeService) {}

  goToUserProfile() {
    this._authFacade.goToUserProfilePage();
  }
}
