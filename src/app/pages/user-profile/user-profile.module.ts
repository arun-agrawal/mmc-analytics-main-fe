import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MercerOSModule } from 'merceros-ui-components';

import { UserProfileComponent } from './user-profile.component';
import { UserProfileRoutingModule } from './user-profile-routing.module';

const COMPONENTS = [
  UserProfileComponent
];

@NgModule({
  imports: [
    CommonModule,
    MercerOSModule,
    UserProfileRoutingModule,
  ],
  declarations: COMPONENTS,
})
export class UserProfileModule { }
