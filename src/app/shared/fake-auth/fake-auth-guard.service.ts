import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { FakeauthService } from './fakeauth.service'

@Injectable()
export class FakeAuthGuardService implements CanActivate {
    constructor(
        public _fakeauthService: FakeauthService,
        public _router: Router) { }
    canActivate(): Promise<boolean> | boolean {
        console.log('auth guard =>', window.location.href);
        return this._fakeauthService.IsLoggedIn().then(isAuthenticated => {
            if (!isAuthenticated) {
                this._router.navigateByUrl('/login');
                return false;
            }

            return true;
        });
    }
}