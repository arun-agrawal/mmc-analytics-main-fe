import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FakeauthService {

  private userKey: string = 'user';
  private validUsers: any[] = [];

  constructor() {
    this.validUsers = [{
      name: "Arun Agrawal",
      email: "arun.agrawal@mercer.com",
      role: "admin"
    },
    {
      name: "John Smith",
      email: "john.smith@mercer.com",
      role: "app-admin"
    }];
  }

  ValidateUser(useremail: string): boolean {
    let user = this.validUsers.find(usr => usr.email.toLowerCase() === useremail.trim().toLowerCase());
    if (user) {
      localStorage.setItem(this.userKey, JSON.stringify(user));
      return true;
    }
    else {
      localStorage.setItem(this.userKey, '');
      return false;
    }
  }

  async IsLoggedIn(): Promise<boolean> {
    let userloggedIn = false;
    let user = localStorage.getItem(this.userKey);

    return new Promise((resolve, reject) => {
      if (user && user.trim() !== '') {
        let userObject = JSON.parse(user);
        userloggedIn = (userObject && userObject.email !== '') ? true : false;
      }
      resolve(userloggedIn);
    });
  }

  GetCurrentUser() {
    let user = localStorage.getItem(this.userKey);
    return (user && user.trim() !== '') ? JSON.parse(user) : null;
  }

  LogOut() {
    localStorage.removeItem(this.userKey);
  }

}
