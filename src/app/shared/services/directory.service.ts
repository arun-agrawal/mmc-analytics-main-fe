import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

import { map } from 'rxjs/operators';
import {environment } from '../../../environments/environment';

@Injectable()
export class DirectoryService {

    directoryServiceEndpoint: string;
    
    getheaders: any;
    postheaders: any;

    constructor(
        private http: HttpClient,
        private router: Router
    ) {
        this.directoryServiceEndpoint =  environment.API + '/peopledirectory';
    }

    initializeHeaders() {
        this.getheaders = new HttpHeaders();
        this.getheaders.append('content-type', 'application/json');

        this.postheaders = new HttpHeaders();
        this.postheaders.append('content-type', 'application/x-www-form-urlencoded');
    }

    GetMatchingColleagues(_firstname: string, _lastname: string): any {

        let paramfname = (_firstname !== null && _firstname !== 'undefined' && _firstname.trim()!=='') ? ('/fname/' + _firstname.trim()) : ('/fname/xxxx');
        let paramlname = (_lastname !== null && _lastname !== 'undefined'  && _lastname.trim()!=='') ? ('/lname/' + _lastname.trim()) : ('/lname/xxxx');
        let url = this.directoryServiceEndpoint + '/search' + paramfname + paramlname;

        console.log('url =>', url)

        let getHeaders = new HttpHeaders();
        getHeaders = getHeaders.append("Content-Type", "text/plain");
        //getHeaders = getHeaders.append("apikey", this.apiKey); 

       return  this.http.get(url.toString(), { headers: getHeaders})
            .pipe(map(res => {
                return res;
            }));
    }

    GetMatchingColleaguesFirstNameContains(_firstname: string): any {
        let paramfname = (_firstname !== null && _firstname !== 'undefined' && _firstname.trim()!=='') ? ('/fname/' + _firstname.trim()) : ('/fname/xxxx');
        let url = this.directoryServiceEndpoint + '/search/contains' + paramfname;

        console.log('url =>', url)

        let getHeaders = new HttpHeaders();
        getHeaders = getHeaders.append("Content-Type", "text/plain");
        //getHeaders = getHeaders.append("apikey", this.apiKey);  

       return  this.http.get(url.toString(), { headers: getHeaders, responseType: 'json' })
            .pipe(map(res => {
                return res;
            }));
    }

    GetMatchingColleaguesLastNameContains(_lastname: string): any {
        let paramlname = (_lastname !== null && _lastname !== 'undefined'  && _lastname.trim()!=='') ? ('/lname/' + _lastname.trim()) : ('/lname/xxxx');
        let url = this.directoryServiceEndpoint + '/search/contains' + paramlname;

        console.log('url =>', url)

        let getHeaders = new HttpHeaders();
        getHeaders = getHeaders.append("Content-Type", "application/json");
        //getHeaders = getHeaders.append("apikey", this.apiKey);

       return  this.http.get(url.toString(), { headers: getHeaders, responseType: 'json' })
            .pipe(map(res => {
                return res;
            }));
    }

}