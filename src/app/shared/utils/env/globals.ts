import { environment } from '../../../../environments/environment';

export function getBackendUrl() {
  // remove trailing slash
  return (environment.API || '').replace(/\/$/, '');
}

export function getBackendQueryUrl() {
  return `${getBackendUrl()}/query`;
}

export function getBackendSearchUrl() {
  // TODO replace url naming to hide underlaying architecture
  return `${getBackendUrl()}/elastic`;
}

/**
 * Given resourceUlr in full url or partial url format, return full url
 */
export function getFullResourceUrl(resourceUrl: string) {
  return resourceUrl ? (/^https?:\/\//i.test(resourceUrl) ? resourceUrl : getBackendUrl() + resourceUrl) : undefined;
}
