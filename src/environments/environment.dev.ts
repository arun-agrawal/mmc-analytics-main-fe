// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// This serves as the base for the dev environment

export const environment = {
  production: false,
  hmr: false,

  // MercerOS app default backend api
  API: 'https://localhost:3000',
  SCRIPT_PATH: 'https://localhost:3000/mmc-web-analytics.js',
  // MFA/MSSO dev defaults start

  mfaDisabled: true,
  mssoDisabled: true,
  mssoFederatedOnly: false,

  // set to true for dev mode (uncomment for testing Authentication package)
  authDevMode: true,

  // Refresh Token Defaults
  jwtTokenExpiresIn: (2 * 60),
  refreshTokenExpiresIn: (10 * 60),

  // Session Timeout Defaults
  sessionTimeoutDisabled: false,
  sessionTimeoutWarningTime: (2 * 60),
  authUserProfileReturnRoute: '/user-profile',

  displayName: 'Kava',
  rpid: 'https://localdev-kava.mercer.com',
  mssoLoginPost: 'https://globalidentity1-uat.mercer.com/GlobalSSOUI/GSSOIdp/Authenticate/LoginPost',
  mssoFailureUrl: 'https://localdev-kava.mercer.com:3000/msso/login/failed',
  appName: 'KAVA',
  loginSuccessRoute: '/dashboard',
  mssoSupportEmail: 'mssosupport@mercer.com',
  mfaSupportEmail: 'mfasupport@mercer.com',
  startingMfaLocale: 'de-DE',

  // MFA/MSSO dev defaults end

  CAAS_API: 'http://localhost:3036/caas/resources/',

};
