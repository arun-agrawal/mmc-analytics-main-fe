export const environment = {
  production: true,
  hmr: false,

  // MercerOS app default backend api
  API: '$(API)',
  SCRIPT_PATH: '$(SCRIPT_PATH)',

  // MFA/MSSO dev defaults start

  mfaDisabled: false,
  mssoDisabled: false,
  mssoFederatedOnly: false,

  // set to true for dev mode
  authDevMode: false,

  // Refresh Token Defaults
  jwtTokenExpiresIn: 2 * 60,
  refreshTokenExpiresIn: 10 * 60,

  // Session Timeout Defaults
  sessionTimeoutDisabled: false,
  sessionTimeoutWarningTime: 2 * 60,
  authUserProfileReturnRoute: '$(AUTH_USER_PROFILE_RETURN_ROUTE)',

  displayName: '$(DISPLAY_NAME)',
  rpid: '$(RPID)',
  mssoLoginPost: '$(MSSO_LOGIN_POST)',
  mssoFailureUrl: '$(MSSO_FAILURE)',
  appName: '$(APP_NAME)',
  loginSuccessRoute: '/dashboard',
  mssoSupportEmail: '$(MSSO_SUPPORT_EMAIL',
  mfaSupportEmail: '$(MFA_SUPPORT_EMAIL)',
  startingMfaLocale: 'en-US',

  // MFA/MSSO dev defaults end

  CAAS_API: 'http://localhost:3036/caas/resources/',
};
